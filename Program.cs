﻿using System;
using System.Reflection;
using Newtonsoft.Json;
using System.Diagnostics;

namespace OtusSerialization
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter number of iterations: ");
            if (int.TryParse(Console.ReadLine(), out int numberOfIterations) == false)
            {
                throw new ArgumentException("String must represent integer number");
            }

            F origInstance = new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5, S1 = "A" };

            Stopwatch stopwatch = new ();

            stopwatch.Start(); // My CSV serialization
            for (int i = 0; i < numberOfIterations; i++)
            {
                string s = Serializer.SerializeFromObject2Csv(origInstance);
                var instanceFromCsv = Serializer.DeserializeFromCsv2Object(s);
            }
            stopwatch.Stop();
            Console.WriteLine($"My CSV serialization: {stopwatch.ElapsedMilliseconds}");

            stopwatch.Start(); // Newtonsoft JSON serialization
            for (int i = 0; i < numberOfIterations; i++)
            {
                string j = JsonConvert.SerializeObject(origInstance);
                var instanceFromJson = JsonConvert.DeserializeObject<F>(j);
            }
            stopwatch.Stop();
            Console.WriteLine($"Newtonsoft JSON serialization: {stopwatch.ElapsedMilliseconds}");
        }
    }

    public class F
    {
        public int i1, i2, i3, i4, i5;
        public string S1 { get; set; }
    }

    public static class Serializer
    {
        private const char _delimiter = ';';

        private enum CsvLines : Byte
        {
            Default = 0,
            Name = 1,
            Fields = 2,
            Properties = 3
        } 

        public static string SerializeFromObject2Csv(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentException("Object is null");
            }

            Type objType = obj.GetType();
            string csv = objType.AssemblyQualifiedName + Environment.NewLine; // 1. Object Name

            if (objType.IsClass)
            {
                foreach (var objField in objType.GetFields()) // 2. Object Fields
                {
                    csv += objField.Name + _delimiter + objField.GetValue(obj) + _delimiter;
                }
                csv += Environment.NewLine;

                foreach (var objProperty in objType.GetProperties()) // 3. Object Properties
                {
                    csv += objProperty.Name + _delimiter + objProperty.GetValue(obj) + _delimiter;
                }
                csv += Environment.NewLine;
            }
            else
            {
                throw new ArgumentException("Object is not a class");
            }

            return csv;
        }

        public static object DeserializeFromCsv2Object(string s)
        {
            CsvLines lineCounter = CsvLines.Default;
            object obj = null;
            Type objType = null;
            FieldInfo fldInf = null;
            PropertyInfo propInf = null;
        
            foreach (string line in s.Split(Environment.NewLine))
            {
                lineCounter++;

                foreach (string field in line.Split(_delimiter))
                {
                    switch (lineCounter)
                    {
                        case CsvLines.Name:
                            obj = Activator.CreateInstance(System.Type.GetType(field, true));
                            objType = obj.GetType();
                            break;

                        case CsvLines.Fields:
                            if (fldInf == null)
                            {
                                fldInf = objType.GetField(field);
                            }
                            else
                            {
                                fldInf.SetValue(obj, Convert.ChangeType(field, fldInf.FieldType));
                                fldInf = null;
                            }
                            break;

                        case CsvLines.Properties:
                            if (propInf == null)
                            {
                                propInf = objType.GetProperty(field);
                            }
                            else
                            {
                                propInf.SetValue(obj, Convert.ChangeType(field, propInf.PropertyType));
                                propInf = null;
                            }
                            break;
                    }
                }
            }

            return obj;
        }
    }
}
